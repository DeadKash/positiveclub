package ru.softc.evotor.positiveclub.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.math.BigDecimal;

/**
 * Created by lifed on 13.10.2017.
 */

public class SettingsHelper {
    private static final String PROPERTY_BONUS_VALUE = "PROPERTY_BONUS_VALUE";
    private static final String PROPERTY_BONUS_BALANCE = "PROPERTY_BONUS_BALANCE";

    private final SharedPreferences preferences;

    public SettingsHelper(Context context) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setBonusValue(BigDecimal bonusValue) {
        preferences.edit()
                .putString(PROPERTY_BONUS_VALUE, bonusValue.toPlainString())
                .apply();
    }

    public BigDecimal getBonusValue() {
        return new BigDecimal(preferences.getString(PROPERTY_BONUS_VALUE, "0"));
    }

    public void setBonusBalance(BigDecimal bonusBalance) {
        preferences.edit()
                .putString(PROPERTY_BONUS_BALANCE, bonusBalance.toPlainString())
                .apply();
    }

    public BigDecimal getBonusBalance() {
        return new BigDecimal(preferences.getString(PROPERTY_BONUS_BALANCE, "0"));
    }
}
