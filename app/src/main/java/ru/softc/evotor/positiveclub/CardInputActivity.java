package ru.softc.evotor.positiveclub;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.evotor.framework.core.IntegrationAppCompatActivity;
import ru.evotor.framework.core.action.event.receipt.changes.position.IPositionChange;
import ru.evotor.framework.core.action.event.receipt.changes.position.PositionEdit;
import ru.evotor.framework.core.action.event.receipt.changes.position.SetExtra;
import ru.evotor.framework.core.action.event.receipt.discount.ReceiptDiscountEvent;
import ru.evotor.framework.core.action.event.receipt.discount.ReceiptDiscountEventResult;
import ru.evotor.framework.receipt.Position;
import ru.evotor.framework.receipt.Receipt;
import ru.evotor.framework.receipt.ReceiptApi;
import ru.evotor.integrations.BarcodeBroadcastReceiver;
import ru.softc.evotor.positiveclub.data.Card;
import ru.softc.evotor.positiveclub.data.DatabaseHelper;
import ru.softc.evotor.positiveclub.data.ResponseWrapper;
import ru.softc.evotor.positiveclub.data.Transaction;
import ru.softc.evotor.positiveclub.dialogs.InputDiscountDialog;
import ru.softc.evotor.positiveclub.helpers.ReceiptExtras;
import ru.softc.evotor.positiveclub.helpers.SettingsHelper;
import ru.softc.evotor.positiveclub.services.BonusActionService;
import ru.softc.evotorhelpers.LogHelper;
import ru.softc.evotorhelpers.ReceiptsHelper;

/**
 * Created by lifed on 05.10.2017.
 */

public class CardInputActivity extends IntegrationAppCompatActivity implements View.OnClickListener {
    private static final int CODE_CREATE_CARD = 1;

    private ServerAPI api;
    private ReceiptDiscountEvent sourceEvent;

    private EditText etCardNumber;
    private Card card;

    private Button btnClear;
    private Button btnRound, btnManual, btnMaximum, btnWithoutBonusPay;
    private ImageView btnBack;

    private TextView tvBonusBalance, tvBonusValue;
    private TextView tvErrorMessage;

    private RelativeLayout layoutCardInfo;

    private ProgressDialog progressDialog;

    private BigDecimal bonusValue, maxBonusValue, roundBonusValue;

    private NumberFormat bonusFormat = new DecimalFormat("0.00");
    private SettingsHelper settingsHelper;

    private Receipt receipt;
    private ReceiptExtras receiptExtras;

    private final BarcodeBroadcastReceiver mBarcodeReceiver = new BarcodeBroadcastReceiver() {
        @Override
        public void onBarcodeReceived(@NotNull String s, @Nullable Context context) {
            processCardNumber(s);
        }
    };

    public static Intent start(Context context) {
        final Intent starter = new Intent(context, CardInputActivity.class);
        return starter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_input);

        api = ServerAPI.retrofit.create(ServerAPI.class);
        initVariables();
    }

    private void initVariables() {
        settingsHelper = new SettingsHelper(this);

        etCardNumber = findViewById(R.id.et_discount);
        etCardNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    if (inputCheck()) {
                        processCardNumber(etCardNumber.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        layoutCardInfo = findViewById(R.id.layout_card_info);
        tvBonusBalance = findViewById(R.id.tv_bonus_balance);
        tvBonusValue = findViewById(R.id.tv_bonus_value);

        btnClear = findViewById(R.id.btn_clear);
        btnWithoutBonusPay = findViewById(R.id.btn_without_bonus_pay);
        btnRound = findViewById(R.id.btn_round);
        btnManual = findViewById(R.id.btn_manual);
        btnMaximum = findViewById(R.id.btn_max);
        btnBack = findViewById(R.id.btn_back);

        tvErrorMessage = findViewById(R.id.tv_error_message);

        btnClear.setOnClickListener(this);
        btnWithoutBonusPay.setOnClickListener(this);
        btnRound.setOnClickListener(this);
        btnManual.setOnClickListener(this);
        btnMaximum.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnClear)) {
            clearCard();
        } else if (v.equals(btnWithoutBonusPay)) {
            transactionCancel(receiptExtras.getTransactionValue());
        } else if (v.equals(btnMaximum)) {
            bonusValue = maxBonusValue;
            transactionHold(bonusValue);
        } else if (v.equals(btnRound)) {
            bonusValue = roundBonusValue;
            transactionHold(bonusValue);
        } else if (v.equals(btnManual)) {
//            InputDiscountDialog dialog = InputDiscountDialog.newInstance(bonusValue, maxBonusValue);
            InputDiscountDialog dialog = InputDiscountDialog.newInstance(receiptExtras.getTransactionValue(), maxBonusValue);
            dialog.show(getFragmentManager(), "InputDiscountDialog");
            dialog.setCallbackListener(new InputDiscountDialog.DialogCallback() {
                @Override
                public void callback(BigDecimal discount) {
                    bonusValue = discount;
                    transactionHold(bonusValue);
                }
            });
        } else if (v.equals(btnBack)) {
            finish();
        }
    }


    private void updateView() {
        if (card == null) {
            layoutCardInfo.setVisibility(View.INVISIBLE);
            tvErrorMessage.setVisibility(View.VISIBLE);
        } else {
            btnMaximum.setText("Максимум\n(" + bonusFormat.format(maxBonusValue) + " бнс)");
            btnRound.setText("Округлить\n(" + bonusFormat.format(roundBonusValue) + " бнс)");

            tvBonusBalance.setText(bonusFormat.format(card.getBalance()) + " бнс");
            tvBonusValue.setText(bonusFormat.format(receiptExtras.getTransactionValue()) + " бнс");

            setErrorMessage("");
            tvErrorMessage.setVisibility(View.INVISIBLE);
            layoutCardInfo.setVisibility(View.VISIBLE);

            if (maxBonusValue.compareTo(BigDecimal.ZERO) == 0)
                btnMaximum.setEnabled(false);
            else
                btnMaximum.setEnabled(true);

            if (roundBonusValue.compareTo(BigDecimal.ZERO) == 0)
                btnRound.setEnabled(false);
            else
                btnRound.setEnabled(true);
        }
    }

    private boolean inputCheck() {
        boolean result = true;
        if (etCardNumber.getText().toString().isEmpty()) {
            etCardNumber.setError(getString(R.string.empty_field));
            result = false;
        }

        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(mBarcodeReceiver, BarcodeBroadcastReceiver.BARCODE_INTENT_FILTER);
        sourceEvent = ReceiptDiscountEvent.create(getSourceBundle());
        receipt = ReceiptApi.getReceipt(this, sourceEvent.getReceiptUuid());
        if (receipt == null) {
            showError(getString(R.string.receipt_is_not_found));
            finish();
        } else {
            receiptExtras = new ReceiptExtras(receipt);
            if (!receiptExtras.isEmpty()) {
//                card = new Card(receiptExtras.getCardNumber(), receiptExtras.getAccountId(),
//                        receiptExtras.getTransactionKey(), settingsHelper.getBonusBalance());
//                processCard(card);
                processCardNumber(receiptExtras.getCardNumber());
            }
        }

        updateView();
    }

    private void setErrorMessage(String message) {
        tvErrorMessage.setText(message);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mBarcodeReceiver);
        super.onPause();
    }

    private void showProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.operation_is_performing));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void addIntegrationResult(JSONObject payments, BigDecimal discount) {
        if (card != null) {
            List<IPositionChange> changes = buildChanges(payments);
            receiptExtras = new ReceiptExtras(card.getNumber(), card.getAccount(), card.getTransactionKey(), discount, payments);
            JSONObject extra = receiptExtras.build();

            final ReceiptDiscountEventResult result = new ReceiptDiscountEventResult(sourceEvent.getDiscount(), new SetExtra(extra), changes);

            setIntegrationResult(result);
            finish();
        }
    }

    private BigDecimal getPayBase() {
        BigDecimal payBase = BigDecimal.ZERO;

        for (Map.Entry<String, String> entry :
                card.getPayments().entrySet()) {
            payBase = payBase.add(new BigDecimal(entry.getValue()));
        }

        return payBase;
    }

    private JSONObject buildReceipt(Receipt receipt) {
        JSONObject object = new JSONObject();

        try {
            object.put("positions", buildPositions(receipt));
        } catch (Exception e) {
            LogHelper.showException(getApplicationContext(), e);
        }

        return object;
    }

    private JSONArray buildPositions(Receipt receipt) {
        JSONArray array = new JSONArray();

        for (Position position :
                receipt.getPositions()) {
            array.put(buildPosition(position));
        }

        return array;
    }

    private JSONObject buildPosition(Position position) {
        JSONObject object = new JSONObject();

        try {
            object.putOpt("uuid", position.getUuid());
            object.putOpt("productUuid", position.getProductUuid());
            object.putOpt("name", position.getName());
            object.putOpt("measureName", position.getMeasureName());
            object.putOpt("measurePrecision", position.getMeasurePrecision());
            object.putOpt("price", position.getPrice().toPlainString());
            object.putOpt("quantity", position.getQuantity().toPlainString());

            BigDecimal positionPayBonus = BigDecimal.ZERO;
            if (receiptExtras.getPayBonuses().has(position.getUuid())) {
                positionPayBonus = new BigDecimal(receiptExtras.getPayBonuses().optString(position.getUuid()));
            }
            object.putOpt("priceWithDiscount", position.getPriceWithDiscountPosition()
                    .add(positionPayBonus.divide(position.getQuantity(), 2, BigDecimal.ROUND_HALF_UP)));
            object.putOpt("barcode", position.getBarcode());
            if (position.getTaxNumber() != null)
                object.putOpt("taxNumber", position.getTaxNumber().toString());
            object.putOpt("productType", position.getProductType().toString());
            object.putOpt("mark", position.getMark());
            if (position.getAlcoholByVolume() != null)
                object.putOpt("alcoholByVolume", position.getAlcoholByVolume().toPlainString());
            object.putOpt("alcoholProductKind", position.getAlcoholProductKindCode());
            if (position.getTareVolume() != null)
                object.putOpt("tareVolume", position.getTareVolume().toPlainString());
        } catch (Exception e) {

        }

        return object;
    }

    private List<IPositionChange> buildChanges(JSONObject payBonuses) {
        List<IPositionChange> changes = new ArrayList<>();

        for (Position position :
                receipt.getPositions()) {
            BigDecimal oldPositionBonusPay = BigDecimal.ZERO;
            BigDecimal newPositionBonusPay = BigDecimal.ZERO;
            if (receiptExtras.getPayBonuses().has(position.getUuid())) {
                oldPositionBonusPay = new BigDecimal(receiptExtras.getPayBonuses().optString(position.getUuid()));
            }
            if (payBonuses.has(position.getUuid())) {
                newPositionBonusPay = new BigDecimal(payBonuses.optString(position.getUuid()));
            }

            BigDecimal priceWithDiscount = position.getPriceWithDiscountPosition()
                    .add(oldPositionBonusPay.divide(position.getQuantity(), 2, BigDecimal.ROUND_HALF_UP))
                    .subtract(newPositionBonusPay.divide(position.getQuantity(), 2, BigDecimal.ROUND_HALF_UP));
            Position newPos = Position.Builder.copyFrom(position)
                    .setPriceWithDiscountPosition(priceWithDiscount).build();
            changes.add(new PositionEdit(newPos));
        }

        return changes;
    }

    private void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    private void processCardNumber(final String number) {
        clearCard();
        etCardNumber.setText(number);
        showProgressDialog();
        JSONObject jsonReceipt = new JSONObject();
        try {
            jsonReceipt.put("receipt", buildReceipt(receipt));
        } catch (Exception e) {
            LogHelper.showException(getApplicationContext(), e);
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonReceipt.toString());
        api.getCardInfo(number, body).enqueue(new Callback<ResponseWrapper<Card>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<Card>> call, Response<ResponseWrapper<Card>> response) {
                dismissProgressDialog();
                try {
                    ResponseWrapper<Card> responseWrapper = response.body();
                    if (responseWrapper.isSucces()) {
                        processCard(responseWrapper.getData());
                    } else {
                        setErrorMessage(responseWrapper.getMessage());
                    }
                } catch (Exception e) {
                    setErrorMessage(getString(R.string.error_getting_data_from_server));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<Card>> call, Throwable t) {
                dismissProgressDialog();
                setErrorMessage(getString(R.string.no_internet_connection));
            }
        });

//        api.getCardInfoBody(number, body).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                dismissProgressDialog();
//                try {
//                    String responseBody = response.body().string();
//                    responseBody.toString();
//                } catch (Exception e) {
//                    setErrorMessage(getString(R.string.error_getting_data_from_server));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                dismissProgressDialog();
//                setErrorMessage(getString(R.string.no_internet_connection));
//            }
//        });
    }

    private void transactionHold(final BigDecimal value) {
        if (!receiptExtras.isEmpty() && receiptExtras.getTransactionValue().compareTo(BigDecimal.ZERO) > 0) {
            JSONObject jsonObject = buildObject(receiptExtras.getTransactionKey(), receiptExtras.getTransactionValue());
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            api.transactionCancel(body).enqueue(new Callback<ResponseWrapper>() {
                @Override
                public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                    if (response.isSuccessful()) {
                        if (response.body().isSucces()) {
                            DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                            Transaction.delete(databaseHelper.getWritableDatabase(), receiptExtras.getTransactionKey());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper> call, Throwable t) {

                }
            });
        }
        if (!receiptExtras.isEmpty() && receiptExtras.getTransactionKey().equals(card.getTransactionKey())) {
            showProgressDialog();
            JSONObject jsonReceipt = new JSONObject();
            try {
                jsonReceipt.put("receipt", buildReceipt(receipt));
            } catch (Exception e) {
                LogHelper.showException(getApplicationContext(), e);
            }
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonReceipt.toString());
            api.getCardInfo(receiptExtras.getCardNumber(), body).enqueue(new Callback<ResponseWrapper<Card>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<Card>> call, Response<ResponseWrapper<Card>> response) {
                    dismissProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().isSucces()) {
                            card = response.body().getData();
                            transactionHold(value);
                        } else {
                            showError(getString(R.string.error_while_get_card_info));
                        }
                    } else {
                        showError(getString(R.string.error_while_get_card_info));
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper<Card>> call, Throwable t) {
                    dismissProgressDialog();
                    showError(getString(R.string.error_while_get_card_info));
                }
            });
            return;
        }
        JSONObject jsonObject = buildObject(card.getTransactionKey(), value);
        try {
            jsonObject.put("receipt", buildReceipt(receipt));
        } catch (Exception e) {
            LogHelper.showException(getApplicationContext(), e);
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        showProgressDialog();
        api.transactionHold(body).enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                dismissProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().isSucces()) {
                        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                        Transaction transaction = new Transaction(card.getTransactionKey(), card.getAccount(), value, Transaction.ACTION_CANCEL);
                        transaction.write(databaseHelper);
                        try {
                            // FIXME: 25.01.2018 very bad things
                            JSONObject data = new JSONObject((response.body().getData()).toString());
                            addIntegrationResult(data.optJSONObject("payments"), value);
                        } catch (Exception e) {
                            LogHelper.showException(getApplicationContext(), e);
                        }
                    } else {
                        showError(getString(R.string.error_hold_bonuses_reason, response.body().getMessage()));
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                dismissProgressDialog();
                showError(getString(R.string.error_hold_bonuses_reason, getString(R.string.no_internet_connection)));
            }
        });
    }

    private void transactionCancel(BigDecimal value) {
        if (receiptExtras.isEmpty()) {
            addIntegrationResult(new JSONObject(), BigDecimal.ZERO);
        } else if (receiptExtras.getTransactionValue().compareTo(BigDecimal.ZERO) > 0) {
            JSONObject jsonObject = buildObject(receiptExtras.getTransactionKey(), value);

            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            showProgressDialog();
            api.transactionCancel(body).enqueue(new Callback<ResponseWrapper>() {
                @Override
                public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                    dismissProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().isSucces()) {
                            DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                            Transaction.delete(databaseHelper.getWritableDatabase(), receiptExtras.getTransactionKey());
                            addIntegrationResult(new JSONObject(), BigDecimal.ZERO);
                        } else {
                            LogHelper.postToastMessage(getApplicationContext(), response.body().getMessage(), 1);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                    dismissProgressDialog();
                    addIntegrationResult(new JSONObject(), BigDecimal.ZERO);
                }
            });
        } else {
            addIntegrationResult(new JSONObject(), BigDecimal.ZERO);
        }
    }

    private JSONObject buildObject(String transactionKey, BigDecimal value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("transaction_key", transactionKey);
            jsonObject.put("account_id", card.getAccount());
            jsonObject.put("value", value.toPlainString());
        } catch (Exception e) {
            LogHelper.showException(this, e);
        }

        return jsonObject;
    }

    private void showError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error)
                .setMessage(message)
                .setNeutralButton(R.string.ok, null)
                .show();
    }

    private void processCard(Card card) {
        if (card == null) {
//            LogHelper.postToastMessage(this, "Карта не найдена", 1);
            return;
        }
        etCardNumber.setText(card.getNumber());
        this.card = card;
        settingsHelper.setBonusBalance(card.getBalance());
        BigDecimal total = ReceiptsHelper.getTotalWithoutDocumentDiscount(receipt);

        maxBonusValue = card.getBalance().min(getPayBase());
        if (maxBonusValue.compareTo(BigDecimal.ZERO) < 0)
            maxBonusValue = BigDecimal.ZERO;
        roundBonusValue = total.subtract(total.setScale(0, BigDecimal.ROUND_FLOOR));

        final ReceiptDiscountEventResult result = new ReceiptDiscountEventResult(sourceEvent.getDiscount(),
                new SetExtra(receiptExtras.build()),
                new ArrayList<IPositionChange>());
        setIntegrationResult(result);

        updateView();
    }

    private void clearCard() {
        etCardNumber.getText().clear();
        tvErrorMessage.setText("");
        card = null;
        final ReceiptDiscountEventResult result = new ReceiptDiscountEventResult(sourceEvent.getDiscount(),
                new SetExtra(null),
                buildChanges(new JSONObject()));
        setIntegrationResult(result);
        updateView();
    }
}
