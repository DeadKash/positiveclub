package ru.softc.evotor.positiveclub.services;

import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.evotor.framework.core.IntegrationService;
import ru.evotor.framework.core.action.event.receipt.before_positions_edited.BeforePositionsEditedEvent;
import ru.evotor.framework.core.action.event.receipt.before_positions_edited.BeforePositionsEditedEventProcessor;
import ru.evotor.framework.core.action.event.receipt.before_positions_edited.BeforePositionsEditedEventResult;
import ru.evotor.framework.core.action.event.receipt.changes.position.IPositionChange;
import ru.evotor.framework.core.action.event.receipt.changes.position.PositionAdd;
import ru.evotor.framework.core.action.event.receipt.changes.position.PositionEdit;
import ru.evotor.framework.core.action.event.receipt.changes.position.PositionRemove;
import ru.evotor.framework.core.action.event.receipt.changes.position.SetExtra;
import ru.evotor.framework.core.action.processor.ActionProcessor;
import ru.evotor.framework.receipt.Position;
import ru.evotor.framework.receipt.Receipt;
import ru.evotor.framework.receipt.ReceiptApi;
import ru.softc.evotor.positiveclub.helpers.ReceiptExtras;
import ru.softc.evotor.positiveclub.helpers.SettingsHelper;
import ru.softc.evotorhelpers.LogHelper;

/**
 * Created by lifed on 29.09.2017.
 */

public class ServiceClearPayBonuses extends IntegrationService {
    @Nullable
    @Override
    protected Map<String, ActionProcessor> createProcessors() {
        final HashMap<String, ActionProcessor> map = new HashMap<>();
        map.put(BeforePositionsEditedEvent.NAME_SELL_RECEIPT, new BeforePositionsEditedEventProcessor() {
            @Override
            public void call(@NonNull String s, @NonNull BeforePositionsEditedEvent beforePositionsEditedEvent, @NonNull Callback callback) {

                try {
                    if (beforePositionsEditedEvent.getChanges().size() != 1) {
                        callback.skip();
                        return;
                    }

                    SettingsHelper settingsHelper = new SettingsHelper(getApplicationContext());
                    List<IPositionChange> changes = beforePositionsEditedEvent.getChanges();
                    Receipt receipt = ReceiptApi.getReceipt(getApplicationContext(), beforePositionsEditedEvent.getReceiptUuid());

                    ReceiptExtras receiptExtras = new ReceiptExtras(receipt);

                    IPositionChange change = changes.get(0);
//                    BigDecimal bonusValue = settingsHelper.getBonusValue();
                    BigDecimal bonusValue = receiptExtras.getTransactionValue();
                    if (bonusValue.compareTo(BigDecimal.ZERO) > 0) {
                        if (change instanceof PositionAdd
                                || change instanceof PositionRemove
                                || change instanceof PositionEdit) {
                            BigDecimal quantity = BigDecimal.ONE;
                            boolean isNewQuantity = false;
                            if (change instanceof PositionEdit) {
                                PositionEdit positionEdit = (PositionEdit) change;
                                Position position = getReceiptPosition(change.getPositionUuid());
                                if (position != null) {
                                    quantity = position.getQuantity();
                                    BigDecimal newQuantity = positionEdit.getPosition().getQuantity();
                                    if (quantity.compareTo(newQuantity) == 0) {
                                        try {
                                            callback.onResult(new BeforePositionsEditedEventResult(changes, new SetExtra(receiptExtras.build())));
                                            return;
                                        } catch (RemoteException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        quantity = newQuantity;
                                        isNewQuantity = true;
                                    }
                                }
                            }
                            // обнуление оплаты бонусами
                            for (int i = 0; i < receipt.getPositions().size(); i++) {
                                Position position = receipt.getPositions().get(i);
                                BigDecimal payBonuses = new BigDecimal(receiptExtras.getPayBonuses().optString(position.getUuid()));
                                BigDecimal priceWithDiscountPosition = position.getTotalWithoutDocumentDiscount()
                                        .add(payBonuses)
                                        .divide(position.getQuantity(), 2, BigDecimal.ROUND_HALF_UP);
                                Position newPosition;
                                if (isNewQuantity &&
                                        change.getPositionUuid().equals(position.getUuid())) {
                                    newPosition = Position.Builder.copyFrom(position)
                                            .setPriceWithDiscountPosition(priceWithDiscountPosition)
                                            .setQuantity(quantity).build();
                                } else {
                                    newPosition = Position.Builder.copyFrom(position)
                                            .setPriceWithDiscountPosition(priceWithDiscountPosition).build();
                                }
                                PositionEdit positionEdit = new PositionEdit(newPosition);
                                changes.add(positionEdit);
                            }

                            startService(BonusActionService.start(getApplicationContext(),
                                    receiptExtras.getTransactionKey(),
                                    receiptExtras.getAccountId(),
                                    receiptExtras.getTransactionValue(),
                                    BonusActionService.ACTION_TRANSACTION_CANCEL));

                            settingsHelper.setBonusValue(BigDecimal.ZERO);
                            receiptExtras.setTransactionValue(BigDecimal.ZERO);
                            receiptExtras.setPayBonuses(new JSONObject());

                            LogHelper.postToastMessage(getApplicationContext(),
                                    "Оплата бонусами отменена", Toast.LENGTH_SHORT);
                        }
                    }

                    callback.onResult(new BeforePositionsEditedEventResult(changes, new SetExtra(receiptExtras.build())));
                } catch (Exception e) {
                    LogHelper.showException(getApplicationContext(), e);
                }


            }
        });

        return map;
    }

    private Position getReceiptPosition(String uuidPosition) {
        Receipt receipt = ReceiptApi.getReceipt(getApplicationContext(), Receipt.Type.SELL);
        for (Position position :
                receipt.getPositions()) {
            if (position.getUuid().equals(uuidPosition))
                return position;
        }

        return null;
    }
}
