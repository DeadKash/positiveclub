package ru.softc.evotor.positiveclub.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.logging.SimpleFormatter;

import ru.softc.evotor.positiveclub.R;

/**
 * Created by lifed on 04.10.2017.
 */

public class InputDiscountDialog extends DialogFragment {

    public static final String ARG_DISCOUNT_MAX = "ARG_DISCOUNT_MAX";
    private static final String ARG_DISCOUNT_CURRENT = "ARG_DISCOUNT_CURRENT";

    private EditText etDiscount;
    private DialogCallback callbackListener;

    private BigDecimal discountMax;
    private BigDecimal discountCurrent;

    private NumberFormat numberFormat = new DecimalFormat("0.00");

    public static InputDiscountDialog newInstance(BigDecimal current, BigDecimal discountMax) {
        final InputDiscountDialog dialog = new InputDiscountDialog();
        final Bundle bundle = new Bundle();
        bundle.putString(ARG_DISCOUNT_MAX, discountMax.toPlainString());
        bundle.putString(ARG_DISCOUNT_CURRENT, current == null ? BigDecimal.ZERO.toPlainString() : current.toPlainString());
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();

        discountMax = new BigDecimal(arguments.getString(ARG_DISCOUNT_MAX));
        discountCurrent = new BigDecimal(arguments.getString(ARG_DISCOUNT_CURRENT));
    }

    @Override
    public void onStart() {
        super.onStart();
        Button positiveButton = ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!inputCheck()) {
                    return;
                }

                BigDecimal discount = new BigDecimal(etDiscount.getText().toString());

                if (callbackListener != null)
                    callbackListener.callback(discount);

                getDialog().dismiss();
            }
        });
    }

    private boolean inputCheck() {
        boolean result = true;

        if (etDiscount.getText().toString().trim().equals("")) {
            etDiscount.setError(getString(R.string.empty_field));
            result = false;
        } else {
            BigDecimal discount = new BigDecimal(etDiscount.getText().toString());
            if (discount.compareTo(BigDecimal.ZERO) <= 0) {
                etDiscount.setError(getString(R.string.error_discount_zero));
                result = false;
            }
            if (discount.compareTo(discountMax) > 0) {
                etDiscount.setError(getString(R.string.error_discount_max));
                result = false;
            }
        }

        return result;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        final View view = layoutInflater.inflate(R.layout.dialog_input_discount, null);

        etDiscount = view.findViewById(R.id.et_discount);
        etDiscount.setHint(numberFormat.format(discountMax));

        builder.setTitle("Введите сумму");
        builder.setView(view);
        builder.setNegativeButton("Отмена", null);
        builder.setPositiveButton("Применить", null);

        etDiscount.setText(discountCurrent.toPlainString());

        return builder.create();
    }

    public void setCallbackListener(DialogCallback callbackListener) {
        this.callbackListener = callbackListener;
    }

    public interface DialogCallback {
        void callback(BigDecimal discount);
    }
}
