package ru.softc.evotor.positiveclub.data;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by lifed on 11.10.2017.
 */

public class Card {
    @SerializedName("transaction_key")
    private String transactionKey;
    @SerializedName("card_number")
    private String number;
    @SerializedName("account_id")
    private String account;
    @SerializedName("balance")
    private BigDecimal balance;
    @SerializedName("payments")
    private HashMap<String, String> payments;
    @SerializedName("bonus")
    private HashMap<String, String> bonus;

    public Card() {
    }

    public Card(String number, String accountId, String transactionKey, BigDecimal balance) {
        this.number = number;
        this.account = accountId;
        this.transactionKey = transactionKey;
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getTransactionKey() {
        return transactionKey;
    }

    public String getNumber() {
        return number;
    }

    public String getAccount() {
        return account;
    }

    public HashMap<String, String> getPayments() {
        return payments;
    }

    public HashMap<String, String> getBonus() {
        return bonus;
    }
}
