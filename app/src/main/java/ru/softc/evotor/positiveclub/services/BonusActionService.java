package ru.softc.evotor.positiveclub.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.evotor.framework.receipt.Receipt;
import ru.evotor.framework.receipt.ReceiptApi;
import ru.softc.evotor.positiveclub.ServerAPI;
import ru.softc.evotor.positiveclub.data.DatabaseHelper;
import ru.softc.evotor.positiveclub.data.ResponseWrapper;
import ru.softc.evotor.positiveclub.data.Transaction;
import ru.softc.evotorhelpers.LogHelper;

/**
 * Created by lifed on 13.10.2017.
 */

public class BonusActionService extends IntentService {

    public static final String ACTION_TRANSACTION_CANCEL = "ACTION_TRANSACTION_CANCEL";
    public static final String ACTION_TRANSACTION_COMMIT = "ACTION_TRANSACTION_COMMIT";

    private static final String EXTRA_TRANSACTION_KEY = "EXTRA_TRANSACTION_KEY";
    private static final String EXTRA_ACCOUNT_ID = "EXTRA_ACCOUNT_ID";
    private static final String EXTRA_BONUS_VALUE = "EXTRA_BONUS_VALUE";

    public BonusActionService() {
        super("BonusActionService");
    }

    public static Intent start(Context context, String transactionKey, String accountId, BigDecimal value, String action) {
        final Intent starter = new Intent(context, BonusActionService.class);

        starter.setAction(action);
        starter.putExtra(EXTRA_TRANSACTION_KEY, transactionKey);
        starter.putExtra(EXTRA_ACCOUNT_ID, accountId);
        starter.putExtra(EXTRA_BONUS_VALUE, value.toPlainString());

        return starter;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String action = intent.getAction();
        Bundle bundle = intent.getExtras();

        final String transactionKey = bundle.getString(EXTRA_TRANSACTION_KEY);
        final String accountId = bundle.getString(EXTRA_ACCOUNT_ID);
        final BigDecimal value = new BigDecimal(bundle.getString(EXTRA_BONUS_VALUE, "0"));

        switch (action) {
            case ACTION_TRANSACTION_COMMIT:
                transactionCommit(transactionKey, accountId, value);
                break;
            case ACTION_TRANSACTION_CANCEL:
                transactionCancel(transactionKey, accountId, value);
                break;
        }
    }

    private void transactionCommit(String transactionKey, String accountId, BigDecimal bonusValue) {
        final ServerAPI api = ServerAPI.retrofit.create(ServerAPI.class);
        try {
            JSONObject jsonObject = buildJsonObject(transactionKey, accountId, bonusValue);
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            Response<ResponseWrapper> response = api.transactionCommit(body).execute();
            if (response.isSuccessful()) {
                if (response.body().isSucces()) {
//                    LogHelper.postToastMessage(getApplicationContext(), "Commited", 0);
                    DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                    Transaction.delete(databaseHelper.getWritableDatabase(), transactionKey);
                } else {
                    DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                    Transaction.setAction(databaseHelper.getWritableDatabase(), transactionKey, Transaction.ACTION_COMMIT);
//                    LogHelper.postToastMessage(getApplicationContext(), response.body().getMessage(), 1);
                }
            }
        } catch (Exception e) {
            DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
            Transaction.setAction(databaseHelper.getWritableDatabase(), transactionKey, Transaction.ACTION_COMMIT);
//            LogHelper.showException(getApplicationContext(), e);
        }
    }

    private void transactionCancel(String transactionKey, String accountId, BigDecimal bonusValue) {
        final ServerAPI api = ServerAPI.retrofit.create(ServerAPI.class);
        try {
            JSONObject jsonObject = buildJsonObject(transactionKey, accountId, bonusValue);
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            Response<ResponseWrapper> response = api.transactionCancel(body).execute();
            if (response.isSuccessful()) {
                if (response.body().isSucces()) {
//                    LogHelper.postToastMessage(getApplicationContext(), "Cancelled", 0);
                    DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                    Transaction.delete(databaseHelper.getWritableDatabase(), transactionKey);
                } else {
//                    LogHelper.postToastMessage(getApplicationContext(), response.body().getMessage(), 1);
                }
            }
        } catch (Exception e) {
//            LogHelper.showException(getApplicationContext(), e);
        }
    }

    private JSONObject buildJsonObject(String transactionKey, String accountId, BigDecimal bonusValue) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("transaction_key", transactionKey);
        jsonObject.put("account_id", accountId);
        jsonObject.put("value", bonusValue.toPlainString());

        return jsonObject;
    }
}
