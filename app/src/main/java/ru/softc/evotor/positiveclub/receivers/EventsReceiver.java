package ru.softc.evotor.positiveclub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import org.json.JSONObject;

import java.math.BigDecimal;

import ru.evotor.framework.core.action.event.receipt.receipt_edited.ReceiptClearedEvent;
import ru.evotor.framework.core.action.event.receipt.receipt_edited.ReceiptClosedEvent;
import ru.evotor.framework.core.action.event.receipt.receipt_edited.ReceiptOpenedEvent;
import ru.evotor.framework.receipt.Receipt;
import ru.evotor.framework.receipt.ReceiptApi;
import ru.softc.evotor.positiveclub.helpers.ReceiptExtras;
import ru.softc.evotor.positiveclub.services.BonusActionService;
import ru.softc.evotor.positiveclub.services.TransactionService;
import ru.softc.evotorhelpers.LogHelper;

public class EventsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        final Bundle bundle = intent.getExtras();

        if (ReceiptOpenedEvent.BROADCAST_ACTION_SELL_RECEIPT.equalsIgnoreCase(action)) {
            context.startService(new Intent(context, TransactionService.class));
        } else if (ReceiptClosedEvent.BROADCAST_ACTION_SELL_RECEIPT_CLOSED.equalsIgnoreCase(action)) {
            /*final SettingsHelper settingsHelper = new SettingsHelper(context);

            final String cardNumber = settingsHelper.getCurrentCardNumber();
            if (cardNumber.isEmpty()) {
                return;
            }*/

            final ReceiptClosedEvent event = ReceiptClosedEvent.create(bundle);

            Receipt receipt = ReceiptApi.getReceipt(context, event.getReceiptUuid());
            if (receipt != null) {
                ReceiptExtras receiptExtras = new ReceiptExtras(receipt);
                if (receiptExtras.getTransactionValue().compareTo(BigDecimal.ZERO) > 0) {
                    context.startService(BonusActionService.start(context, receiptExtras.getTransactionKey(),
                            receiptExtras.getAccountId(), receiptExtras.getTransactionValue(), BonusActionService.ACTION_TRANSACTION_COMMIT));
                }
            }
        }
    }
}
