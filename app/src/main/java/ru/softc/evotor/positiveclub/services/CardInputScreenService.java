package ru.softc.evotor.positiveclub.services;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import ru.evotor.framework.core.IntegrationService;
import ru.evotor.framework.core.action.event.receipt.discount.ReceiptDiscountEvent;
import ru.evotor.framework.core.action.event.receipt.discount.ReceiptDiscountEventProcessor;
import ru.evotor.framework.core.action.processor.ActionProcessor;
import ru.softc.evotor.positiveclub.CardInputActivity;

/**
 * Created by lifed on 05.10.2017.
 */

public class CardInputScreenService extends IntegrationService {
    @Nullable
    @Override
    protected Map<String, ActionProcessor> createProcessors() {
        final HashMap<String, ActionProcessor> map = new HashMap<>();

        map.put(ReceiptDiscountEvent.NAME_SELL_RECEIPT, new ReceiptDiscountEventProcessor() {
            @Override
            public void call(@NonNull String action, @NonNull ReceiptDiscountEvent event, @NonNull Callback callback) {
                try {
                    callback.startActivity(CardInputActivity.start(getApplicationContext()));
//                    success.onResult(new ReceiptDiscountEventResult(event.getDiscount(), null, new ArrayList<IPositionChange>()));
                } catch (Exception e) {
//                    LogHelper.showException(getApplicationContext(), e);
                }
            }
        });

        return map;
    }
}
