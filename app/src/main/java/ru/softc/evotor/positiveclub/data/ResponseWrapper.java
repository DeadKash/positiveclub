package ru.softc.evotor.positiveclub.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lifed on 09.10.2017.
 */

public class ResponseWrapper<T> {
    public static final String RESULT_SUCCESS = "success";
    public static final String RESULT_ERROR = "error";

    private String result;
    private int code;
    private String message;
    @SerializedName("data")
    private T data;

    public ResponseWrapper() {
    }

    public boolean isSucces() {
        return result.equals(RESULT_SUCCESS);
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getResult() {
        return result;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}
