package ru.softc.evotor.positiveclub.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.List;

import ru.softc.evotor.positiveclub.data.DatabaseHelper;
import ru.softc.evotor.positiveclub.data.Transaction;

/**
 * Created by lifed on 13.10.2017.
 */

public class TransactionService extends IntentService {

    public TransactionService() {
        super("TransactionService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        List<Transaction> transactions = Transaction.select(databaseHelper.getWritableDatabase());
        for (Transaction transaction :
                transactions) {
            if (transaction.getAction() == Transaction.ACTION_CANCEL) {
                startService(BonusActionService.start(getApplicationContext(), transaction.getTransactionKey(), transaction.getAccountId(),
                        transaction.getValue(), BonusActionService.ACTION_TRANSACTION_CANCEL));
            } else {
                startService(BonusActionService.start(getApplicationContext(), transaction.getTransactionKey(), transaction.getAccountId(),
                        transaction.getValue(), BonusActionService.ACTION_TRANSACTION_COMMIT));
            }
        }
    }
}
