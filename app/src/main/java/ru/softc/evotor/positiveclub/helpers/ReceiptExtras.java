package ru.softc.evotor.positiveclub.helpers;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.math.BigDecimal;

import ru.evotor.framework.receipt.Receipt;
import ru.softc.evotor.positiveclub.ServerAPI;
import ru.softc.evotorhelpers.LogHelper;

/**
 * Created by lifed on 13.10.2017.
 */

public class ReceiptExtras {

    private static final String FIELD_CARD_NUMBER = "card_number";
    private static final String FIELD_ACCOUNT_ID = "account_id";
    private static final String FIELD_TRANSACTION_KEY = "transaction_key";
    private static final String FIELD_TRANSACTION_VALUE = "transaction_value";
    private static final String FIELD_PAY_BONUSES = "pay_bonuses";

    private String cardNumber;
    private String accountId;
    private String transactionKey;
    private BigDecimal transactionValue;
    private JSONObject payBonuses;

    public ReceiptExtras(String cardNumber, String accountId, String transactionKey, BigDecimal transactionValue, JSONObject payments) {
        this.cardNumber = cardNumber;
        this.accountId = accountId;
        this.transactionKey = transactionKey;
        this.transactionValue = transactionValue;
        this.payBonuses = payments;
    }

    public ReceiptExtras(@NonNull Receipt receipt) {
        JSONObject object = new JSONObject();
        try {
            object = new JSONObject(receipt.getHeader().getExtra());
        } catch (Exception e) {
        }
        cardNumber = object.optString(FIELD_CARD_NUMBER);
        accountId = object.optString(FIELD_ACCOUNT_ID);
        transactionKey = object.optString(FIELD_TRANSACTION_KEY);
        transactionValue = new BigDecimal(object.optString(FIELD_TRANSACTION_VALUE, "0"));

        if (object.has(FIELD_PAY_BONUSES)) {
            payBonuses = object.optJSONObject(FIELD_PAY_BONUSES);
        } else {
            payBonuses = new JSONObject();
        }
    }

    public boolean isEmpty() {
        return transactionKey.isEmpty();
    }

    public JSONObject build() {
        JSONObject object = new JSONObject();

        try {
            object.put(FIELD_CARD_NUMBER, cardNumber);
            object.put(FIELD_ACCOUNT_ID, accountId);
            object.put(FIELD_TRANSACTION_KEY, transactionKey);
            object.put(FIELD_TRANSACTION_VALUE, transactionValue.toPlainString());
            object.put(FIELD_PAY_BONUSES, payBonuses);
        } catch (Exception e) {
        }

        return object;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getTransactionKey() {
        return transactionKey;
    }

    public BigDecimal getTransactionValue() {
        return transactionValue;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setTransactionKey(String transactionKey) {
        this.transactionKey = transactionKey;
    }

    public void setTransactionValue(BigDecimal transactionValue) {
        this.transactionValue = transactionValue;
    }

    public JSONObject getPayBonuses() {
        return payBonuses;
    }

    public void setPayBonuses(JSONObject payBonuses) {
        this.payBonuses = payBonuses;
    }
}
