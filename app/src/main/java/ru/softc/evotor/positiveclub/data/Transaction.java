package ru.softc.evotor.positiveclub.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ru.softc.evotorhelpers.data.BaseEntity;

/**
 * Created by lifed on 13.10.2017.
 */

public class Transaction extends BaseEntity {
    private static final String TABLE_NAME = "Transactions";

    public static final int ACTION_CANCEL = 0;
    public static final int ACTION_COMMIT = 1;

    public static final String COL_TRANSACTION_KEY = "transaction_key";
    public static final String COL_ACCOUNT_ID = "account_id";
    public static final String COL_VALUE = "value";
    public static final String COL_ACTION = "action";

    public static final String[] COLUMNS = new String[]{
            COL_ID,
            COL_UUID,
            COL_TRANSACTION_KEY,
            COL_ACCOUNT_ID,
            COL_VALUE,
            COL_ACTION
    };

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
            "(" + COL_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            COL_UUID + " TEXT NOT NULL UNIQUE, " +
            COL_TRANSACTION_KEY + " TEXT NOT NULL, " +
            COL_ACCOUNT_ID + " TEXT NOT NULL, " +
            COL_VALUE + " TEXT NOT NULL, " +
            COL_ACTION + " INTEGER NOT NULL DEFAULT " + ACTION_CANCEL +
            ");";

    @NonNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    private String transactionKey;
    private String accountId;
    private BigDecimal value;
    private int action;

    public Transaction(String transactionKey, String accountId, BigDecimal value, int action) {
        super();
        this.transactionKey = transactionKey;
        this.accountId = accountId;
        this.value = value;
        this.action = action;
    }

    public Transaction(Cursor cursor) {
        super(cursor.getLong(0), cursor.getString(1));

        this.transactionKey = cursor.getString(2);
        this.accountId = cursor.getString(3);
        this.value = new BigDecimal(cursor.getString(4));
        this.action = cursor.getInt(5);
    }

    public String getTransactionKey() {
        return transactionKey;
    }

    public void setTransactionKey(String transactionKey) {
        this.transactionKey = transactionKey;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    @Override
    protected ContentValues toContentValues() {
        ContentValues values = super.toContentValues();
        values.put(COL_TRANSACTION_KEY, transactionKey);
        values.put(COL_ACCOUNT_ID, accountId);
        values.put(COL_VALUE, value.toPlainString());
        values.put(COL_ACTION, action);

        return values;
    }

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static List<Transaction> select(SQLiteDatabase db) {
        final ArrayList<Transaction> result = new ArrayList<>();
        final Cursor cursor = db.query(TABLE_NAME, COLUMNS, null, null, null, null, null);
        try {
            while (cursor.moveToNext()) {
                result.add(new Transaction(cursor));
            }
        } finally {
            cursor.close();
        }
        return result;
    }

    public static void delete(SQLiteDatabase db, String transactionKey) {
        db.delete(TABLE_NAME, COL_TRANSACTION_KEY + " = ?", new String[]{transactionKey});
    }

    public static void setAction(SQLiteDatabase db, String transactionKey, int action) {
        ContentValues values = new ContentValues();
        values.put(COL_ACTION, action);
        db.update(TABLE_NAME, values, COL_TRANSACTION_KEY + " = ?", new String[]{String.valueOf(transactionKey)});
    }
}
