package ru.softc.evotor.positiveclub;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.softc.evotor.positiveclub.data.Card;
import ru.softc.evotor.positiveclub.data.ResponseWrapper;

/**
 * Created by lifed on 18.09.2017.
 */

public interface ServerAPI {
    String BASE_URL = "https://dclubs.ru/evotor/positive/api/device/";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @POST("card/{number}")
    Call<ResponseWrapper<Card>> getCardInfo(@Path("number") String number, @Body RequestBody jsonObject);

    @POST("card/{number}")
    Call<ResponseBody> getCardInfoBody(@Path("number") String number, @Body RequestBody jsonObject);

    @POST("transaction/hold")
    Call<ResponseWrapper> transactionHold(@Body RequestBody jsonObject);

    @POST("transaction/cancel")
    Call<ResponseWrapper> transactionCancel(@Body RequestBody jsonObject);

    @POST("transaction/commit")
    Call<ResponseWrapper> transactionCommit(@Body RequestBody jsonObject);
}
