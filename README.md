# ПозитивКлуб #

Система лояльности аналогичная онлайн-режиму "ДисконтКлуб Бонусы"

## Данные, добавляемые в Extra чека ##

* card_number - номер карты
* account_id - номер счета
* transaction_key - код ключ транзакции
* transaction_value - сумма списания >= 0

## REST API ##

Базовый URL для всех запросов: https://dclubs.ru/evotor/positive/api/device/

Все ответы обернуты в конструкцию вида:

 * result - success или error
 * code - код ошибки
 * message - сообщение об ошибке
 * data - данные
 
### POST /card/:number ###

Получить информацию о карте с номером number.

#### Запрос ####

* receipt
    * positions[]
        * productUuid
        * quantity
        * price
        * priceWithDiscount
        * productCode
        * barcode
        * productType - enum(NORMAL, ALCOHOL_NOT_MARKED, ALCOHOL_MARKED, SERVICE)
        * alcoholByVolume
        * tareVolume
        * alcoholProductKindCode

#### Ответ ####

* transaction_key - ключ операции
* card_number - номер каты из запроса
* account_id - номер счета
* balance - текущий остаток
* payments:
    * $uuid -> $payment - uuid позиции чека -> масимально возможная сумма оплаты (без учета баланса).
* bonus:
    * $uuid -> $bonus - uuid позиции чека -> сумма бонусов к наислению.

### POST /transaction/hold ###

Резервирование средств.

#### Запрос ####

 * transaction_key - ключ операции (из информации о карте);
 * account_id - номер счета;
 * value - сумма;
 * receipt (см. POST /card/:number).

#### Ответ ####

* payments:
    * $uuid -> $payment - uuid позиции чека -> масимально возможная сумма оплаты (без учета баланса).
* bonus:
    * $uuid -> $bonus - uuid позиции чека -> сумма бонусов к наислению.

### POST /transaction/cancel ###

Отмена резерва.

#### Запрос ####

 * transaction_key - ключ операции (из информации о карте);
 * account_id - номер счета;
 * value - сумма.

#### Ответ ####

Пустой.

### POST /transaction/commit ###

Подтверждение списания.

#### Запрос ####

 * transaction_key - ключ операции (из информации о карте);
 * account_id - номер счета;
 * value - сумма.

#### Ответ ####

Пустой.
